<?php
/**
 * @file
 *
 * @author    Tsuy Ito <tsuy.ito@getunik.com>
 * @copyright Copyright (c) 2014 copyright
 */

namespace Drupal\raisenow_payment;


class RaiseNowCreditCardForm extends \Drupal\payment_forms\CreditCardForm {
    static protected $issuers = array();
    static protected $cvc_label = array(
        'vis'    => 'CVV2 (Card Verification Value 2)',
        'amex'   => 'CID (Card Identification Number)',
        'eca'    => 'CVC2 (Card Validation Code 2)',
        'jcb'    => 'CSC (Card Security Code)'
    );

    public function getForm(array &$form, array &$form_state) {
        //RaiseNowCreditCardForm::$issuers =
        $controller = $form_state['payment']->method->controller;
        RaiseNowCreditCardForm::$issuers = array();

        $allAvailableCardTypes = $form_state['payment']->method->controller_data['payment_method_types'];
        foreach($allAvailableCardTypes as $key => $pmt) {
            RaiseNowCreditCardForm::$issuers[$key] = $controller->creditCardTypes[$key];
        }

        parent::getForm($form, $form_state);
        $payment = &$form_state['payment'];

        drupal_add_js(CommonForm::getSettings($payment), 'setting');
        CommonForm::addRaiseNowBridge($form);
        CommonForm::addEppTransactionIdField($form);

        return $form;
    }

    public function validateForm(array &$element, array &$form_state) {
        // raisenow takes care of the real validation, client-side.
        CommonForm::addEppTransactionIdToPaymentMethodData($element, $form_state);
    }
}
