<?php
/**
 * @file
 *
 * @author    Tsuy Ito <tsuy.ito@getunik.com>
 * @copyright Copyright (c) 2014 copyright
 */
namespace Drupal\raisenow_payment;

class CreditCardController extends \Drupal\raisenow_payment\CommonController implements \Drupal\webform_paymethod_select\PaymentRecurrentController {

    public $creditCardTypes = array(
        'vis' => 'VISA',
        'eca' => 'MasterCard',
        'amx' => 'American Express',
        'din' => 'Diners Club',
        'jcb' => 'JCB',
    );

    public function __construct() {
        parent::__construct();

        $this->controller_data_defaults = array_merge($this->controller_data_defaults, array('payment_method_types' => array()));

        $this->title = t('RaiseNow Credit Card');
        $this->form = new \Drupal\raisenow_payment\RaiseNowCreditCardForm();
    }

    public function configurationForm(array $form, array &$form_state) {
        $controller_data = $form_state['payment_method']->controller_data + $this->controller_data_defaults;

        $form = parent::configurationForm($form, $form_state);
        $form['payment_method_types'] = array(
            '#default_value' => $controller_data['payment_method_types'],
            '#options' => $this->creditCardTypes,
            '#type' => 'checkboxes',
            '#title' => t('Credit Card Types')
        );


    return $form;
  }
}
