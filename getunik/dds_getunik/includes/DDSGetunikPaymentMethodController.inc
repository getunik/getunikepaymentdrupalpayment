<?php

/**
 * Contains DDSGetunikPaymentMethodController.
 */

/**
 * An getunik payment method.
 */
class DDSGetunikPaymentMethodController extends PaymentMethodController {

  const SECRET = 'DLlKar5Al47WeGNw8mrKawFeFYEmY3xd5v2b2w1DKUL0t6KiXJEAxpgkgg0k';

  /**
   * The production mode
   */
  const PRODUCTION_MODE = 0;

  /**
   * The testing mode
   */
  const TESTING_MODE = 1;

  /**
   * The test mode
   */
  const TEST_MODE = self::TESTING_MODE;

  /**
   * The server URL.
   */
  const SERVER_URL = 'https://dds-pay.getunik.net/epayment/api/step/pay/merchant/';

  public $controller_data_defaults = array(
    'username' => '',
    'password' => '',
    'apikey' => '',
    'test_mode' => self::TEST_MODE,
    'payment_method_brand' => '',
  );

  public $payment_method_configuration_form_elements_callback = 'dds_getunik_payment_method_configuration_form_elements';

  public $title = 'DDS Getunik';

  function __construct() {
    $currency_codes = array('BRL', 'CHF', 'EUR', 'USD');
    $this->currencies = array_fill_keys($currency_codes, array());
  }

  /**
   * Implements PaymentMethodController::validate().
   */
  function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
    //parent::validate($payment, $payment_method, $strict);

  }

  /**
   * Implements PaymentMethodController::execute().
   */
  function execute(Payment $payment) {
    entity_save('payment', $payment);
    $_SESSION['dds_getunik_pid'] = $payment->pid;
    drupal_goto('dds_getunik/redirect/' . $payment->pid);
  }

  /**
   * Returns the API server URL.
   *
   * @throws InvalidArgumentException
   *
   *
   * @return string
   */
  function serverURL() {
  	return url($this::SERVER_URL, array(
        'external' => TRUE,
      ));
  }

  /**
   * Returns a map of getunik statuses to Payment statuses.
   *
   * @return array
   *   Keys are getunik statuses, values are Payment statuses.
   */
  function statusMap() {
    // @todo Find more detailed payment status descriptions than those in the
    // parameter cookbook and update the statuses accordingly, by possibly
    // creating getunik-specific Payment payment statuses.
    return array(
      'aborted_by_user' => PAYMENT_STATUS_CANCELLED,
      'cancel' => PAYMENT_STATUS_CANCELLED,
      'unknown' => PAYMENT_STATUS_UNKNOWN,
      'error' => PAYMENT_STATUS_FAILED,
      'success' => PAYMENT_STATUS_SUCCESS,
    );
  }

  /**
   * Converts an getunik status to a Payment status.
   *
   * @param string|int $dds_getunik_status
   *
   * @return string
   */
  function convertStatus($dds_getunik_status) {
	$status_map = $this->statusMap();
    if (isset($status_map[$dds_getunik_status])) {
    	return $status_map[$dds_getunik_status];
    }
    return PAYMENT_STATUS_UNKNOWN;
  }

  /**
   * Maps payment methods and brands.
   *
   * @return array
   *   Keys are payment method names. Values array arrays that contain brand names.
   */
  function brandMap() {
    static $map = NULL;

    if (is_null($map)) {
      $map = array(
        'amx' => 'American Express',
        'din' => 'Diners Club',
        'eca' => 'MasterCard',
        'vis' => 'Visa',
        'pfc' => 'PostFinance Card',
        'pef' => 'PostFinance E-Finance',
        'pex' => 'PayPal',
        'sms' => 'SMS',
      );
    }

    return $map;
  }

  /**
   * Returns payment method brand options.
   *
   * @return array
   *   Keys are payment method/brand names. Values array arrays that contain brand names.
   */
  function brandOptions() {
    $options = $this->brandMap();
    natcasesort($options);
    return $options;
  }

  /**
   * Sets up redirect POST data.
   *
   * @param Payment $payment
   *
   * @return array
   *   Keys are POST parameter names, values are values.
   */
  function redirectData(Payment $payment) {

    dpm($payment, "payment");
    $return_url = url('dds_getunik/return', array(
      'absolute' => TRUE,
    ));

    $paymentMethod = 'vis';
    if(property_exists($payment, 'field_dds_getunik_pm')) {
      foreach($payment->field_dds_getunik_pm as $key => $val) {
        if(array_key_exists(0, $val)) {
          $paymentMethod = $val[0]['value'];
        }
      }
    }
    $paymentMethod = $payment->method->controller_data['payment_method_brand']? $payment->method->controller_data['payment_method_brand'] : $paymentMethod;

    $data = array(
      'amount' => intval(currency_load($payment->currency_code)->roundAmount($payment->totalAmount(TRUE) * 100)),
      'apikey' => $payment->method->controller_data['apikey'],
      'stored_customer_order_id' => $payment->pid,
      'currency' => !$payment->currency_code || $payment->currency_code === 'XXX'? 'chf' : $payment->currency_code,
      'language' => preg_replace('/\_[a-zA-Z]*/', '', $this->locale()),
      //'email' => user_load($payment->uid)->mail,
      'stored_message' => $payment->description,
      'success_url' => $return_url,
      'error_url' => $return_url,
      'cancel_url' => $return_url,
      'payment_method' => $paymentMethod,
      'test_mode' => $payment->method->controller_data['test_mode']? 'true' : 'false',
    );

    switch ($paymentMethod) {
      case 'din':
      case 'vis':
      case 'eca':
      case 'amx':
        $data['card_holder_name'] =  !empty($payment->method_data['card_holder_name'])? $payment->method_data['card_holder_name'] : null;
        $data['expy'] =  !empty($payment->method_data['expy'])? $payment->method_data['expy'] : null;
        $data['expm'] =  !empty($payment->method_data['expm'])? $payment->method_data['expm'] : null;
        $data['cardno'] =  !empty($payment->method_data['cardno'])? $payment->method_data['cardno'] : null;
        $data['cvv'] =  !empty($payment->method_data['cvv'])? $payment->method_data['cvv'] : null;
        $data['reqtype'] =  'CAA';
        break;
      case 'sms':
         $data['msisdn'] =  !empty($payment->method_data['msisdn'])? $payment->method_data['msisdn'] : null;
        break;
      case 'pex':
      case 'pfc':
      case 'pef':
        break;
    }

    $data['stored_transaction_type'] = 'shop';
    $data['stored_shipping_method'] = 'NORMAL';


    $order = commerce_order_load($payment->context_data['order_id']);
    dpm($order, "order");

    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    $billing_address = $wrapper->commerce_customer_billing->commerce_customer_address->value();
    dpm($billing_address, "billing_address");

    $data['stored_customer_email'] = $wrapper->mail->value();
    $data['stored_customer_firstname'] = $billing_address['first_name'];
    $data['stored_customer_lastname'] = $billing_address['last_name'];
    $data['stored_customer_street'] = $billing_address['thoroughfare'];
    //$data['stored_customer_street_number'] = '12';
    $data['stored_customer_address_addendum'] = '';
    $data['stored_customer_pobox'] = '';
    $data['stored_customer_zip_code'] = $billing_address['postal_code'];
    $data['stored_customer_city'] = $billing_address['locality'];
    $data['stored_customer_language'] = 'de';
    $data['stored_customer_country'] = $billing_address['country'];


    /*$shipping_address = $wrapper->commerce_customer_shipping->commerce_customer_address->value();
    dpm($shipping_address, "shipping_address");

    $data['stored_recipient_email'] = $wrapper->mail->value();
    $data['stored_recipient_firstname'] = $billing_address['first_name'];
    $data['stored_recipient_lastname'] = $billing_address['last_name'];
    $data['stored_recipient_street'] = $billing_address['thoroughfare'];
    $data['stored_recipient_street_number'] = '';
    $data['stored_recipient_address_addendum'] = '';
    $data['stored_recipient_pobox'] = '';
    $data['stored_recipient_zip_code'] = $billing_address['postal_code'];
    $data['stored_recipient_city'] = $billing_address['locality'];
    $data['stored_recipient_language'] = '';*/

    dpm($payment->line_items, "line items");

    foreach (array_values($payment->line_items) as $i => $line_item) {
      // $data['ITEMID' . $i] = $line_item->name;
      // $data['ITEMNAME' . $i] = t($line_item->description, $line_item->description_arguments);
      // $data['ITEMPRICE' . $i] = $line_item->unitAmount(TRUE);
      // $data['ITEMQUANT' . $i] = $line_item->quantity;

      $sku = ($line_item->description === 'Standard shipping' ? 'PORTO' : $line_item->description);

      $data['stored_ordered_product_' . $i] = json_encode(array(
        'productCode' => $sku, // description field actually contains the SKU
        'unitPrice' => $line_item->unitAmount(TRUE) * 100,
        'quantity' => $line_item->quantity,
        'lineTotal' => $line_item->totalAmount(TRUE) * 100,
        'discount' => 0,
      ));
    }

    // '{"productCode": "#aBasket[i].product_wwfid#", "unitPrice": #Round(100 * aBasket[i].price.normal.value)#, "quantity": #aBasket[i].quantity#, "lineTotal": #Round(100 * aBasket[i].lineTotal)#, "discount": #aBasket[i].discount#}'
    drupal_alter('dds_getunik_redirect_data', $data, $payment);
    //$data['SHASIGN'] = $this->signIncomingData($data, $payment->method);
    $data['hmac'] = $this->signIncomingData($data, $payment->method);

    dpm($data, "data");
    //throw new \Exception("nope");

    return $data;
  }

  /**
   * Signs POST message data.
   *
   * @param array $data
   *   Keys are POST parameter names, values are values.
   * @param array $signature_parameters
   *   The names of the parameters that are be used to generate the signature.
   * @param string $passphrase
   * @param string $algorithm
   *
   * @return string
   *   The signature.
   */
  function signData(array $data, array $signature_parameters, $secret, $algorithm) {
    // Filter parameters that are not needed for the signature.
    ksort($data);
    $signature_data_string = '';
    foreach ($signature_parameters as $signature_parameter) {
      // the replacement of '*XX*' by a digit regex would indicate to me that you plan to make those
      // signature parameters configurable at some point. if so, this will most likely break or cause
      // odd behavior as soon as someone decides to add a '.' or '\' in their parameter names...
      // why not just add the regex directly to the signature parameter definition?
      $signature_parameter_pattern = '/^' . str_replace('*XX*', '\d+?', $signature_parameter) . '$/i';
      foreach ($data as $data_parameter => $value) {
        if (strlen($value) && preg_match($signature_parameter_pattern, $data_parameter)) {
          // one could argue that you don't get any benefit out of adding the secret to _every_ parameter;
          // the only effect it has is to make the hash computation (very, very slightly ;) slower.
          // wouldn't it be enough to add the secret to the start or end of the string?
          //$signature_data_string .= strtoupper($data_parameter) . '=' .  strtoupper($value) . $passphrase;
          $signature_data_string .= $value;
        }
      }
    }
    dpm($signature_data_string);
    return hash_hmac($algorithm, $signature_data_string, $secret); //strtoupper(hash($algorithm, $signature_data_string));
  }

  /**
   * Signs incoming POST message data.
   *
   * @param array $data
   *   Keys are POST parameter names, values are values.
   * @param PaymentMethod $payment_method
   *
   * @return string
   *   The signature.
   */
  function signIncomingData(array $data, PaymentMethod $payment_method) {
    return $this->signData($data, $this->signatureParameters(), /*$payment_method->controller_data['apikey']*/self::SECRET, 'sha1');
  }

  /**
   * Returns the names of to be encrypted incoming parameters.
   *
   * @see TBD
   *
   * @return array
   */
  function signatureParameters() {
    return array(
      'AMOUNT',
      'CURRENCY',
      'TEST_MODE',
      'ITEM_COUNT',
      'ITEMATTRIBUTES*XX*',
      'ITEMCATEGORY*XX*',
      'ITEMCOMMENTS*XX*',
      'ITEMDESC*XX*',
      'ITEMDISCOUNT*XX*',
      'ITEMID*XX*',
      'ITEMNAME*XX*',
      'ITEMPRICE*XX*',
      'ITEMQUANT*XX*',
      'ITEMQUANTORIG*XX*',
      'ITEMUNITOFMEASURE*XX*',
      'ITEMVAT*XX*',
      'ITEMVATCODE*XX*',
      'ITEMWEIGHT*XX*',
      'MAXITEMQUANT*XX*',
      'NETAMOUNT',
      'OPERATION',
      'ORDERID',
      'ORDERSHIPCOST',
      'ORDERSHIPMETH',
      'ORDERSHIPTAX',
      'ORDERSHIPTAXCODE',
      //'PAYMENT_METHOD',
      //'STORED_CUSTOMER_ORDER_ID',
      'STORED_ORDERED_PRODUCT_*XX*',
    );
  }

  /**
   * Finds the locale to use for getunik communication.
   *
   * @return string
   *   A locale identifier.
   */
  function locale() {
    global $language;

    $supported_locales = $this->supportedLocales();
    $country_codes = array(variable_get('site_default_country', ''), strtoupper($language->language));
    foreach ($country_codes as $country_code) {
      $locale = $language->language . '_' . $country_code;
      if (in_array($locale, $supported_locales)) {
        return $locale;
      }
    }
    return 'en_US';
  }

  /**
   * Returns an array of supported locale identifiers.
   *
   * @return array
   */
  function supportedLocales() {
    return array(
      'en_GB',
      'en_US',
      'fr_FR',
      'de_DE',
      'it_IT',
      'es_ES',
    );
  }

  /**
   * Gets Getunik feedback data.
   *
   * @return array
   */
  function feedbackData() {
    $data_raw = array_merge($_GET, $_POST);
    unset($data_raw['q']);
    $data = array();
    // As opposed to what the Ogone e-Commerce Advanced technical documentation
    // says, not all parameter names are all-caps, even though they should be
    // to generate a correct signature.
    foreach ($data_raw as $parameter => $value) {
      $data[strtoupper($parameter)] = $value;
    }

    return $data;
  }

  /**
   * Processes Getunik feedback data.
   *
   * @return NULL
   */
  function processFeedback(array $data, Payment $payment) {
    $payment->setStatus(new PaymentStatusItem($this->convertStatus($data['EPAYMENT_STATUS'])));
    entity_save('payment', $payment);
    module_invoke_all('dds_getunik_feedback', $data, $payment);
    $payment->finish();
  }
}
