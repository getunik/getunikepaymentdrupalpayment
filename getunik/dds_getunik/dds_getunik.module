<?php

/**
 * @file
 * Contains hook implementations and global functions.
 */

/**
 * Implements hook_menu().
 */
function dds_getunik_menu() {
  $items['dds_getunik/redirect/%entity_object'] = array(
    'load arguments' => array('payment'),
    'title' => 'Go to payment server',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dds_getunik_form_redirect', 2),
    'access callback' => 'dds_getunik_form_redirect_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );
  $items['dds_getunik/return'] = array(
    'title' => 'DDS getunik return url',
    'page callback' => 'dds_getunik_return',
    'access callback' => 'dds_getunik_return_access',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_payment_method_controller_info().
 */
function dds_getunik_payment_method_controller_info() {
  return array('DDSGetunikPaymentMethodController');
}

/**
 * Implements hook_entity_load().
 */
function dds_getunik_entity_load(array $entities, $entity_type) {
  if ($entity_type == 'payment_method') {
    $pmids = array();
    foreach ($entities as $payment_method) {
      if ($payment_method->controller->name == 'DDSGetunikPaymentMethodController') {
        $pmids[] = $payment_method->pmid;
      }
    }
    if ($pmids) {
      $query = db_select('dds_getunik_payment_method')
        ->fields('dds_getunik_payment_method')
        ->condition('pmid', $pmids);
      $result = $query->execute();
      while ($data = $result->fetchAssoc()) {
        $payment_method = $entities[$data['pmid']];
        $payment_method->controller_data = (array) $data;
        unset($payment_method->controller_data['pmid']);
      }
    }
  }
}

/**
 * Implements hook_entity_insert().
 */
function dds_getunik_payment_method_insert(PaymentMethod $payment_method) {
  if ($payment_method->controller->name == 'DDSGetunikPaymentMethodController') {
    $values = $payment_method->controller_data += $payment_method->controller->controller_data_defaults;
    $values['pmid'] = $payment_method->pmid;
    drupal_write_record('dds_getunik_payment_method', $values);
  }
}

/**
 * Implements hook_entity_update().
 */
function dds_getunik_payment_method_update(PaymentMethod $payment_method) {
  if ($payment_method->controller->name == 'DDSGetunikPaymentMethodController') {
    $values = $payment_method->controller_data += $payment_method->controller->controller_data_defaults;
    $values['pmid'] = $payment_method->pmid;
    drupal_write_record('dds_getunik_payment_method', $values, 'pmid');
  }
}

/**
 * Implements hook_entity_delete().
 */
function dds_getunik_payment_method_delete($payment_method) {
  if ($payment_method->controller->name == 'DDSGetunikPaymentMethodController') {
    db_delete('dds_getunik_payment_method')
      ->condition('pmid', $payment_method->pmid)
      ->execute();
  }
}

/**
 * Form build callback: implements
 * PaymentMethodController::payment_method_configuration_form_elements_callback.
 */
function dds_getunik_payment_method_configuration_form_elements(array $form, array &$form_state) {
  $payment_method = $form_state['payment_method'];
  $controller = $payment_method->controller;
  $controller_data = $payment_method->controller_data + $controller->controller_data_defaults;
//print_r($controller_data);
  $elements['username'] = array(
    '#default_value' => $controller_data['username'],
    '#description' => t('The username.'),
    '#required' => FALSE,
    '#title' => t('Username'),
    '#type' => 'textfield',
  );
  $elements['password'] = array(
    '#default_value' => $controller_data['password'],
    '#description' => t('The password.'),
    '#required' => FALSE,
    '#title' => t('Password'),
    '#type' => 'textfield',
  );
  $elements['apikey'] = array(
    '#default_value' => $controller_data['apikey'],
    '#description' => t('The api key'),
    '#required' => TRUE,
    '#title' => t('Api key'),
    '#type' => 'textfield',
  );
  $elements['test_mode'] = array(
    '#default_value' => $controller_data['test_mode'],
    '#options' => array(
      $controller::PRODUCTION_MODE => ('Production'),
      $controller::TESTING_MODE => ('Testing'),
    ),
    '#required' => TRUE,
    '#title' => t('Test mode'),
    '#type' => 'radios',
  );
  $elements['payment_method_brand'] = array(
    '#default_value' => $controller_data['payment_method_brand'],
    '#description' => t('If no brand is selected, the developer has to add the payment method as custom field field_dds_getunik_pm to the form.'),
    '#empty_value' => '',
    '#options' => $controller->brandOptions(),
    '#title' => t('getunik payment method brand'),
    '#type' => 'select',
  );

  return $elements;
}

/**
 * Implements form validate callback for
 * dds_getunik_payment_method_configuration_form_elements().
 */
function dds_getunik_payment_method_configuration_form_elements_validate(array $element, array &$form_state) {
  $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);

  $controller_data = &$form_state['payment_method']->controller_data;
  $controller_data['payment_method_brand'] = $values['payment_method_brand'];
  $controller_data['apikey'] = $values['apikey'];
  $controller_data['username'] = $values['username'];
  $controller_data['password'] = $values['password'];
  $controller_data['test_mode'] = $values['test_mode'];
}

/**
 * Form build callback: the redirect form.
 */
function dds_getunik_form_redirect(array $form, array &$form_state, Payment $payment) {
  $form['#action'] = $payment->method->controller->serverURL() . $payment->method->controller_data['apikey'];
  dpm($form, "form");
  foreach ($payment->method->controller->redirectData($payment) as $parameter => $value) {
    $form[$parameter] = array(
      '#type' => 'hidden',
      '#value' => $value,
    );
  }
  $form['message'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('You will be redirected to the off-site payment server to authorize the payment.') . '</p>',
  );
  // We need form submission as quickly as possible, so use light inline code.
  $form['js'] = array(
    '#type' => 'markup',
    '#markup' => '<script type="text/javascript">document.getElementById(\'dds-getunik-form-redirect\').submit();</script>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 * Access callback for the redirect form.
 *
 * @param Payment $payment
 *   The payment to check access to.
 * @param object $user
 *   An optional user to check access for. If NULL, then the currently logged
 *   in user is used.
 *
 * @return bool
 */
function dds_getunik_form_redirect_access(Payment $payment, $account = NULL) {
  global $user;

  return is_a($payment->method->controller, 'DDSGetunikPaymentMethodController')
    && payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_PENDING)
    && isset($_SESSION['dds_getunik_pid']) && $_SESSION['dds_getunik_pid'] == $payment->pid;
}

/**
 * Processes a return/feedback request.
 *
 * @return NULL
 */
function dds_getunik_return() {
  $controller = payment_method_controller_load('DDSGetunikPaymentMethodController');
  $data = $controller->feedbackData();
  $payment = entity_load_single('payment', $data['STORED_CUSTOMER_ORDER_ID']);
  $payment->method->controller->processFeedback($data, $payment);
}

/**
 * Checks access for the return URL.
 *
 * @return bool
 */
function dds_getunik_return_access() {
  $controller = payment_method_controller_load('DDSGetunikPaymentMethodController');
  $data = $controller->feedbackData();
  return TRUE;
  if (isset($data['STORED_CUSTOMER_ORDER_ID']) && isset($data['SHASIGN'])) {
    $payment = entity_load_single('payment', $data['STORED_CUSTOMER_ORDER_ID']);
    if ($payment) {
      return $data['SHASIGN'] == $payment->method->controller->signIncomingData($data, $payment->method);
    }
  }
  return FALSE;
}
