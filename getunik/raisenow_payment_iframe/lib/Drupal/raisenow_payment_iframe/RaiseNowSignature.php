<?php

namespace Drupal\raisenow_payment_iframe;

/**
 * Helper class for payment signatures
 */
class RaiseNowSignature {

  private static $defaultSignParameters = array(
    'EPP_TRANSACTION_ID', // response only
    'HMAC', // response only
    'AMOUNT',
    'CURRENCY',
    'TEST_MODE',
    'EPAYMENT_STATUS', // response only
    'ITEM_COUNT',
    'ITEMATTRIBUTES*XX*',
    'ITEMCATEGORY*XX*',
    'ITEMCOMMENTS*XX*',
    'ITEMDESC*XX*',
    'ITEMDISCOUNT*XX*',
    'ITEMID*XX*',
    'ITEMNAME*XX*',
    'ITEMPRICE*XX*',
    'ITEMQUANT*XX*',
    'ITEMQUANTORIG*XX*',
    'ITEMUNITOFMEASURE*XX*',
    'ITEMVAT*XX*',
    'ITEMVATCODE*XX*',
    'ITEMWEIGHT*XX*',
    'MAXITEMQUANT*XX*',
    'NETAMOUNT',
    'OPERATION',
    'ORDERID',
    'ORDERSHIPCOST',
    'ORDERSHIPMETH',
    'ORDERSHIPTAX',
    'ORDERSHIPTAXCODE',
    //'PAYMENT_METHOD',
    //'STORED_CUSTOMER_ORDER_ID',
    'STORED_ORDER_PROMOTION_CODE',
    'STORED_ORDERED_PRODUCT_*XX*',
    'STORED_ROUNDUP_DONATION',
  );

  /**
   *@param string $secret
   */
  private $secret;
  /**
   * @param string $algorithm
   */
  private $algorithm;
  /**
   * @param array $signature_parameters
   *   The names of the parameters that are be used to generate the signature.
   */
  private $signParameters;

  public function __construct($secret, $algorithm) {
    $this->secret = $secret;
    $this->algorithm = $algorithm;
    $this->signParameters = self::$defaultSignParameters;
  }

  public function getSignParameters() {
    return $this->signParameters;
  }

  public function setSignParameters($signParameters) {
    $this->signParameters = $signParameters;
  }


  /**
   * Signs payment message data.
   *
   * @param array $data
   *   Keys are POST parameter names, values are values.
   *
   * @return string
   *   The signature.
   */
  function signData(array $data) {
    // Filter parameters that are not needed for the signature.
    ksort($data);
    $signature_data_string = '';
    foreach ($this->signParameters as $parameter) {
      // the replacement of '*XX*' by a digit regex would indicate to me that you plan to make those
      // signature parameters configurable at some point. if so, this will most likely break or cause
      // odd behavior as soon as someone decides to add a '.' or '\' in their parameter names...
      // why not just add the regex directly to the signature parameter definition?
      $signature_parameter_pattern = '/^' . str_replace('*XX*', '\d+?', $parameter) . '$/i';

      foreach ($data as $data_parameter => $value) {
        if (strlen($value) && preg_match($signature_parameter_pattern, $data_parameter)) {
          $signature_data_string .= $value;
        }
      }
    }

    watchdog('raisenow_payment_iframe', 'Payment signature data string: "@data"' , array('@data' => $signature_data_string), WATCHDOG_DEBUG);

    return hash_hmac($this->algorithm, $signature_data_string, $this->secret);
  }
}
