<?php

/**
 * Implements hook_default_rules_configuration_alter().
 */
function raisenow_payment_iframe_commerce_default_rules_configuration_alter(&$configs) {
  // add check_transaction_double_confirmation action to the commerce rule that
  // puts the order in the 'pending' state
  if (isset($configs['commerce_checkout_order_status_update'])) {
    $configs['commerce_checkout_order_status_update']
      ->action('check_transaction_double_confirmation', array(
        'commerce_order:select' => 'commerce-order',
      ));
  }
}
