<?php

/**
 * Implements hook_rules_event_info().
 */
function raisenow_payment_iframe_commerce_rules_event_info() {
  $items = array(
    'raisenow_payment_iframe_commerce_confirmation' => array(
      'label' => t('After the RaiseNow payment has been confirmed'),
      'group' => t('Payment'),
      'variables' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Shopping cart order'),
        ),
      ),
    ),
  );

  return $items;
}


/**
 * Implement hook_rules_action_info().
 */
function raisenow_payment_iframe_commerce_rules_action_info() {
  return array(
    'check_transaction_double_confirmation' => array(
      'label' => t('Checks and triggers the RaiseNow confirmation event'),
      'group' => t('Payment'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order to update'),
        ),
      ),
    ),
  );
}
