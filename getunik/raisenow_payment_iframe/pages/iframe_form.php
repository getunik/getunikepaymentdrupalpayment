<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Payment</title>
</head>
<body>
	<form id="payment-form" method="POST" action="<?php echo $payment_api; ?>">
		<?php foreach ($payment_parameters as $key => $value): ?>
			<input type="hidden" name="<?php echo $key; ?>" value="<?php echo preg_replace('/"/', '&quot;', $value); ?>" />
		<?php endforeach; ?>
	</form>
	<script type="text/javascript">
		// once the form is here, auto-submit it to start the payment process
		document.getElementById("payment-form").submit();
	</script>
</body>
</html>
