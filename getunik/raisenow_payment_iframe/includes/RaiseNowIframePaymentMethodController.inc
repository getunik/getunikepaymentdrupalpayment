<?php

/**
 * Contains RaiseNowIframePaymentMethodController.
 */

use Drupal\raisenow_payment_iframe\RaiseNowSignature;

/**
 * A RaiseNow payment method.
 */
class RaiseNowIframePaymentMethodController extends PaymentMethodController {

  /**
   * The production mode
   */
  const PRODUCTION_MODE = 0;

  /**
   * The testing mode
   */
  const TESTING_MODE = 1;

  /**
   * The test mode
   */
  const TEST_MODE = self::TESTING_MODE;

  /**
   * The server URL.
   */
  const DEFAULT_BASE_API_URL = 'https://api.raisenow.com/epayment/api/step/pay/merchant/';

  public $controller_data_defaults = array(
    'username' => '',
    'password' => '',
    'apikey' => '',
    'sign_secret' => '',
    'sign_algorithm' => 'sha1',
    'test_mode' => self::TEST_MODE,
    'payment_method_brand' => '',
  );

  public $payment_method_configuration_form_elements_callback = 'raisenow_payment_iframe_payment_method_configuration_form_elements';

  public $title = 'RaiseNow iframe';

  function __construct() {
    $currency_codes = array('BRL', 'CHF', 'EUR', 'USD');
    $this->currencies = array_fill_keys($currency_codes, array());
  }

  /**
   * Implements PaymentMethodController::validate().
   */
  function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
    parent::validate($payment, $payment_method, $strict);
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  function execute(Payment $payment) {
    entity_save('payment', $payment);
    // this ties the payment to the current user (which is not necessarily logged in)
    $_SESSION['raisenow_payment_iframe_pid_' . $payment->pid] = TRUE;
    drupal_goto('raisenow/payment/container/' . $payment->pid);
  }

  public function getApiUrl(Payment $payment) {
    $baseUrl = variable_get('raisenow_payment_base_api_url', $this::DEFAULT_BASE_API_URL);
    return url($baseUrl . $payment->method->controller_data['apikey'], array('external' => TRUE));
  }

  /**
   * Returns a map of RaiseNow statuses to Payment statuses.
   *
   * @return array
   *   Keys are RaiseNow statuses, values are Payment statuses.
   */
  function statusMap() {
    // @todo Find more detailed payment status descriptions than those in the
    // parameter cookbook and update the statuses accordingly, by possibly
    // creating RaiseNow-specific Payment payment statuses.
    return array(
      'aborted_by_user' => PAYMENT_STATUS_CANCELLED,
      'cancel' => PAYMENT_STATUS_CANCELLED,
      'unknown' => PAYMENT_STATUS_UNKNOWN,
      'error' => PAYMENT_STATUS_FAILED,
      'success' => PAYMENT_STATUS_SUCCESS,
      'pending' => PAYMENT_STATUS_PENDING,
    );
  }

  /**
   * Converts an RaiseNow status to a Payment status.
   *
   * @param string|int $status
   *
   * @return string
   */
  function convertStatus($status) {
    $status_map = $this->statusMap();
    if (isset($status_map[$status])) {
      return $status_map[$status];
    }
    return PAYMENT_STATUS_UNKNOWN;
  }

  /**
   * Maps payment methods and brands.
   *
   * @return array
   *   Keys are payment method names. Values array arrays that contain brand names.
   */
  function brandMap() {
    static $map = NULL;

    if (is_null($map)) {
      $map = array(
        'amx' => 'American Express',
        'din' => 'Diners Club',
        'eca' => 'MasterCard',
        'vis' => 'Visa',
        'pfc' => 'PostFinance Card',
        'pef' => 'PostFinance E-Finance',
        'pex' => 'PayPal',
        'sms' => 'SMS',
        'dd' => 'Direct Debit / LSV',
        'es' => 'Payment Slip',
        'ezs' => 'Payment Slip (with reference no.)',
      );
    }

    return $map;
  }

  /**
   * Returns payment method brand options.
   *
   * @return array
   *   Keys are payment method/brand names. Values array arrays that contain brand names.
   */
  function brandOptions() {
    $options = $this->brandMap();
    natcasesort($options);
    return $options;
  }

  function getSignature(Payment $payment, $data) {
    $signer = new RaiseNowSignature($payment->method->controller_data['sign_secret'], $payment->method->controller_data['sign_algorithm']);
    return $signer->signData($data);
  }

  function getPaymentData(Payment $payment) {
    $url_options = array('absolute' => TRUE, 'https' => TRUE);
    $return_url = url('raisenow/payment/return/' . $payment->pid, $url_options);

    $paymentMethod = 'vis';
    if(property_exists($payment, 'field_rnw_iframe_pm')) {
      foreach($payment->field_rnw_iframe_pm as $key => $val) {
        if(array_key_exists(0, $val)) {
          $paymentMethod = $val[0]['value'];
        }
      }
    }

    $paymentMethod = $payment->method->controller_data['payment_method_brand'] ? $payment->method->controller_data['payment_method_brand'] : $paymentMethod;

    $stylesheet = raisenow_payment_get_stylesheet('default');
    $stylesheet_url_options = $url_options;
    if (variable_get('raisenow_payment_iframe_debug', FALSE)) {
      $stylesheet_url_options['query'] = array('use_proxy' => 'false');
    }
    $stylesheetUrl = url('raisenow/payment/styles/default/styles.css', $stylesheet_url_options);

    $data = array(
      'css_url' => ($stylesheet == NULL ? NULL : $stylesheetUrl),
      'amount' => intval(currency_load($payment->currency_code)->roundAmount($payment->totalAmount(TRUE) * 100)),
      'apikey' => $payment->method->controller_data['apikey'],
      'stored_customer_pid' => $payment->pid,
      'currency' => !$payment->currency_code || $payment->currency_code === 'XXX'? 'chf' : $payment->currency_code,
      'language' => preg_replace('/\_[a-zA-Z]*/', '', $this->locale()),
      //'email' => user_load($payment->uid)->mail,
      'stored_message' => $payment->description,
      'success_url' => $return_url,
      'error_url' => $return_url,
      'cancel_url' => $return_url,
      'payment_method' => $paymentMethod,
      'test_mode' => $payment->method->controller_data['test_mode']? 'true' : 'false',
    );

    switch ($paymentMethod) {
      case 'din':
      case 'vis':
      case 'eca':
      case 'amx':
        $data['card_holder_name'] =  !empty($payment->method_data['card_holder_name'])? $payment->method_data['card_holder_name'] : null;
        $data['expy'] =  !empty($payment->method_data['expy'])? $payment->method_data['expy'] : null;
        $data['expm'] =  !empty($payment->method_data['expm'])? $payment->method_data['expm'] : null;
        $data['cardno'] =  !empty($payment->method_data['cardno'])? $payment->method_data['cardno'] : null;
        $data['cvv'] =  !empty($payment->method_data['cvv'])? $payment->method_data['cvv'] : null;
        $data['reqtype'] =  'CAA';
        break;
      case 'sms':
         $data['msisdn'] =  !empty($payment->method_data['msisdn'])? $payment->method_data['msisdn'] : null;
        break;
      case 'es':
      case 'ezs':
        for ($i = 1; $i < 5; $i++) {
          $data['payer_line' . $i] = !empty($payment->method_data['payer_line' . $i]) ? empty($payment->method_data['payer_line' . $i]) : null;
        }
        break;
      case 'pex':
      case 'pfc':
      case 'pef':
      case 'dd':
        break;
    }

    drupal_alter('raisenow_payment_iframe_data', $data, $payment);

    if (!empty($payment->method->controller_data['sign_secret']) && !empty($payment->method->controller_data['sign_algorithm'])) {
      $data['hmac'] = $this->getSignature($payment, $data);
    }

    return $data;
  }

  /**
   * Checks the GET and POST data of the current payment return request for validity
   * and returns a list of validation errors that were detected.
   *
   * @param Payment $payment
   *   The payment instance associated with this return request
   */
  public function validateReturnData(Payment $payment) {
    $errors = array();
    $data = $this->getRequestData();

    if (isset($data['hmac'])) {
      $payment_data = $this->getPaymentData($payment);
      $payment_hmac = $payment_data['hmac'];
      $response_hmac = $this->getSignature($payment, $data);


      if ($data['hmac'] != $payment_hmac) {
        $errors[] = 'payment HMAC mismatch with returned "hmac" parameter';
      }

      if (isset($data['response_hmac']) && $data['response_hmac'] != $response_hmac) {
        $errors[] = 'response HMAC validation failed';
      }
    }

    return $errors;
  }

  /**
   * Finds the locale to use for RaiseNow communication.
   *
   * @return string
   *   A locale identifier.
   */
  function locale() {
    global $language;

    $supported_locales = $this->supportedLocales();
    $country_codes = array(variable_get('site_default_country', ''), strtoupper($language->language));
    foreach ($country_codes as $country_code) {
      $locale = $language->language . '_' . $country_code;
      if (in_array($locale, $supported_locales)) {
        return $locale;
      }
    }
    return 'en_US';
  }

  /**
   * Returns an array of supported locale identifiers.
   *
   * @return array
   */
  function supportedLocales() {
    return array(
      'en_GB',
      'en_US',
      'fr_FR',
      'de_DE',
      'it_IT',
      'es_ES',
    );
  }

  /**
   * Gets Getunik feedback data.
   *
   * @return array
   */
  function getRequestData() {
    $data_raw = array_merge($_GET, $_POST);
    unset($data_raw['q']);
    // $data = array();

    // the RNW HMAC mechanism processes parameters as-is; transforming them to uppercase
    // is not necessary.
    // // As opposed to what the Ogone e-Commerce Advanced technical documentation
    // // says, not all parameter names are all-caps, even though they should be
    // // to generate a correct signature.
    // foreach ($data_raw as $parameter => $value) {
    //   $data[strtoupper($parameter)] = $value;
    // }

    // return $data;

    return $data_raw;
  }

  public function processPaymentReturn(Payment $payment) {
    $errors = $this->validateReturnData($payment);
    $data = $this->getRequestData();

    if (isset($data['epp_transaction_id'])) {
      $payment->context_data['transaction_id'] = $data['epp_transaction_id'];
    }

    if (count($errors) > 0) {
      $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_FAILED));
      foreach ($errors as $error) {
        _raisenow_payment_append_error($payment, $error);
        watchdog('raisenow_payment_iframe', 'Payment return validation error: @msg' , array('@msg' => $error));
      }
    } else {
      if (!isset($data['epayment_status'])) {
        watchdog('raisenow_payment_iframe', 'Payment return is missing a status (pid: @pid)' , array('@pid' => $payment->pid), WATCHDOG_ERROR);
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_UNKNOWN));
      } else {
        $payment->setStatus(new PaymentStatusItem($this->convertStatus($data['epayment_status'])));
      }

      if (isset($data['error_type'])) {
        _raisenow_payment_append_error($payment, $data['error_type'] . ' | ' . $data['error_detail']);
      }

      module_invoke_all('raisenow_payment_iframe_return', $data, $payment);
    }

    entity_save('payment', $payment);
  }
}
